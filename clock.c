# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <string.h>
# define _GNU_SOURCE
void clock(int x,int duration)
{
	FILE *stream;
    char read[1024];

    char* curr_time;
    char* curr_date;

    char file_name[1024];
    //to get time /proc/driver/rtc file is used
    strcpy(file_name,"/proc/driver/rtc");

    stream = fopen(file_name, "r");
    fgets(read,100000,stream);
    if(stream == NULL)
    {
      printf("stream is NULL\n");
      return ;
    }
    int dur=0;
    while (stream!=NULL && dur<duration) 
    {
        char* separate;
        separate = strtok(read," \n\t");

        if(strcmp(separate,"rtc_time")==0)
        {
        	separate = strtok(NULL,"\n\t ");
        	separate = strtok(NULL,"\n\t ");	
        	curr_time = strdup(separate);
        }

    	fgets(read,100000,stream);
        separate = strtok(read," \n\t");

      	if(strcmp(separate,"rtc_date")==0)
        {
        	separate = strtok(NULL,"\n\t ");
        	separate = strtok(NULL,"\n\t ");	
        	curr_date = strdup(separate);
       	}

      	separate = strtok(NULL,"\n\t ");
        printf("%s  %s\n",curr_date,curr_time);

        fclose(stream);
        sleep(x);
        dur+=x;
        stream = fopen(file_name, "r");
    	fgets(read,100000,stream);
    }
    return ;
}
