# include <stdio.h>
# include <unistd.h>
# include <string.h>
# include <sys/types.h>
# include <dirent.h>
# include <sys/utsname.h>
# include "globalvar.h"

void builtin_command()
{
	if(strcmp(command[0],"echo")==0)
	{
		for(int i=1;i<ind;i++)
		{
			printf("%s ",command[i]);
		}
		printf("\n");
	}
	else
	{
		if(strcmp(command[0],"pwd")==0)
		{
			printf("%s\n",currdir_abs);
		}
		else
		{
			if(strcmp(command[0],"cd")==0)
			{   
				int open_dir=0;
				dirstream = opendir(currdir_abs);
				if(command[1]==NULL || strcmp(command[1],"~")==0 || strcmp(command[1],".")==0)
				{
					if(command[1]==NULL || strcmp(command[1],".")!=0)
					{
						currdir_rel[0]='~';
						currdir_rel[1]='\0';
						strcpy(currdir_abs,main_dir);
					}
				}
				else
				{
					// for command starting from ~ the abs path is started from the main_dir
					if(command[1][0]=='~')
					{
						strcpy(currdir_rel,"~");
						strcpy(currdir_abs,main_dir);
						//strndup adds the null termintion 
						command[1]=strndup(command[1]+2,strlen(command[1])-2);
					}
					// for command ./ the path is started from current dir 
					if(command[1][0]=='.' && command[1][1]=='/')
					{
						command[1] = strndup(command[1]+2,strlen(command[1])-2);
						printf("%s\n",command[1]);	
					}

					// for command ".." the path returns to the parent of the current directory 
					if(strcmp(command[1],"..")==0)
					{
						int i;
						for(i=strlen(currdir_abs);i>=0;i--)
						{
							if(currdir_abs[i]=='/')
							{
								currdir_abs[i]='\0';
								break;
							}
						}
						if(strcmp(currdir_rel,"~")==0)
						{
							strcpy(currdir_rel,currdir_abs);
						}
						else
						{
							for(i=strlen(currdir_rel);i>=0;i--)
							{
								if(currdir_rel[i]=='/')
								{
									currdir_rel[i]='\0';
									break;
								}
							}
						}
						
					}
					//checking in the current directory if the directory about to
					//open is present or not
					char command_break[100];
					int k=0,i,j=0;
					command_break[0]='\0';
					for(i=k;i<strlen(command[1]);i++,j++)
					{
						if(command[1][i]=='/')
							break;
						command_break[j]=command[1][i];
					}
					k=i+1;
					command_break[j]='\0';
					// going one by one inside the directory given in the path taken as input
					while(command_break[0]!='\0')
					{
						// printf("%s\n",currdir_abs);
						dirstream = opendir(currdir_abs);
						if(dirstream!=NULL)
						{
							direc=readdir(dirstream);
							while(direc!=NULL)
							{
								if(strcmp(direc->d_name,command_break)==0 && strcmp(command_break,"..")!=0)
								{
									open_dir=1;
								}   
								direc=readdir(dirstream);
							}
							closedir(dirstream);
						}

						//changing the path of currdir relatively and absolutely
						if(open_dir==1)
						{
							int i,j,k;

							for(i=0;currdir_abs[i]!='\0';i++);
								currdir_abs[i]='/';
							i++;

							for(k=0;currdir_rel[k]!='\0';k++);
								currdir_rel[k]='/';
							k++;

							for(j=0;command_break[j]!='\0';j++,i++,k++)
							{
								currdir_abs[i]=command_break[j];
								currdir_rel[k]=command_break[j];
							}
							currdir_abs[i]='\0';
							currdir_rel[k]='\0';
							open_dir=0;

						}
						else
						{
							if(strcmp(command_break,"..")!=0)
								printf("invalid directory %s\n",command_break);
							break;
						}
						if(strcmp(currdir_rel,main_dir)==0)
						{
							currdir_rel[0]='~';
							currdir_rel[1]='\0';
						}
						command_break[0]='\0';
						j=0;
						for(i=k;i<strlen(command[1]);i++,j++)
						{
							if(command[1][i]=='/')
								break;
							command_break[j]=command[1][i];
						}
						k=i+1;
						command_break[j]='\0';
					}
				}
			}
		}
	}
}