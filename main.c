# include <stdio.h>
# include <unistd.h>
# include <string.h>
# include <sys/types.h>
# include <dirent.h>
# include <sys/utsname.h>
# include <sys/wait.h>
# include <signal.h>
# include "globalvar.h"
# include "builtin_command.h"
# include "ls_command.h"
# include "bg.h"
# include "pinfo.h"
# include "reminder.h"
# include "clock.h"


int main()
{
	currdir_abs[0]='\0';
	currdir_rel[0]='~';
	currdir_rel[1]='\0';
	main_dir[0]='\0';

	char* sysname;
	getcwd(main_dir,1024);
	getcwd(currdir_abs,1024);
	gethostname(hostname,1000);
	sysname  = strdup(hostname);
	hostname1 = strtok(hostname,"-");
	
	int pid=-2,wpid;
	int p_wait=1;
	int status =0;
	int parent_pid = getpid();

	int grouppid;
	int child_pid;
	int next=0;
	
	while(1)
	{
		int k=0;
		printf("<%s@%s:%s> ",hostname1,sysname,currdir_rel);
		// printf("<%s@%s:%s> ",hostname1,devicename.sysname,currdir_rel);
		fgets(sc_commands,1024,stdin);
		commands[0] = strtok(sc_commands,";");
		int no_com=0;
		while(commands[no_com]!=NULL)
		{
			no_com++;
			commands[no_com] = strtok(NULL,";");
		}
		k=0;
		while(k<no_com)
		{
			pid=0;
			p_wait=1;
			flag[0]='\0';
			command[0] = strtok(commands[k]," \"\'\n\t");
			ind=0;
			while(command[ind]!=NULL)
			{
				ind++;
				command[ind] = strtok(NULL," \"\'\n\t");
			}
			if(command[0]!=NULL && strcmp(command[0],"remindme")==0)
				strcat(command[ind-1],"&");

			if(commands[k][0]!='\n' && command[0]!=NULL && command[ind-1][strlen(command[ind-1])-1]=='&')
			{
				p_wait=0;//parent dosen't have to wait
				command[ind-1][strlen(command[ind-1])-1]='\0';					
				// bg_process[next1] = strdup(command[0]);
				pid=fork();
				child_pid = getpid();
				if(pid!=0)
				{
					bgpid[next]=pid;
					bg_process[next]=strdup(command[0]);
					// printf("command %s\n",command[0]);
					next++;
					k++;//if parent process then it simply continues with the next command
					continue;
				}
				else
				{
					printf("Background process id %d\n",child_pid);
					// bgpid[next1] = child_pid;
					// next1++;
				}

			}

			if(pid==0)
			{
				setpgid(child_pid,1);
				grouppid = getpgid(getpid());//setting the group pid for all the child processes
				if(commands[k][0]!='\n' && command[0]!=NULL)
				{
					if(strcmp(command[0],"pinfo")==0 ||strcmp(command[0],"echo")==0 || strcmp(command[0],"pwd")==0 || strcmp(command[0],"cd")==0)
					{
						if(strcmp(command[0],"pinfo")==0)
						{
							if(command[1]==NULL)
							{
								char pinfo_pid[10];
								sprintf(pinfo_pid,"%d",parent_pid);
								pinfo(pinfo_pid);
							}
							else
								pinfo(command[1]);
						}
						else
							builtin_command(); 
					}
					else
					{
						if(strcmp(command[0],"remindme")==0)
						{
							// printf("reminder\n");
							char arg[1024];
							arg[0]='\0';
							for(int all=2;all<ind;all++)
							{
								// printf("c%s\n",command[all]);
								strcat(arg,command[all]);
								strcat(arg," ");
							}
							// printf("arg %s\n",arg);
							int timer;
							sscanf(command[1],"%d",&timer);
							reminder(timer,arg);
							return 0 ;
						}
						else
						{
							if(strcmp(command[0],"clock")==0)
							{
								int interval;
								int duration;
								sscanf(command[2],"%d",&interval);
								sscanf(command[4],"%d",&duration);
								clock(interval,duration);
							}
							else 
							{
							if(strcmp(command[0],"ls")==0)
							{
								ls();
							}
							else
							{
								if(p_wait==1)
								{
									// to open other program from this process
									// p_wait is 0 only when there is some background process
									// for those processes a new process is already started through fork 
									pid = fork();

								}

								if(pid==0)
								{
									// setting the group pid of this process to the same as other child process
									setpgid(getpid(),grouppid);
									background();
								}
								else
								{
									while ((wpid = wait(&status)) > 0);//the parent process waits for the foreground processes if any
								}
								
							}
							}
						}
					}
								
				}
			}
			else
			{
				if(p_wait==1)
				{
					while ((wpid = wait(&status)) > 0);//the parent process waits for the foreground processes if any
				 }
			}

			if(p_wait==0)//p_wait==0 only when there is a child process therefore it breaks the loop to terminate the child
				break;

			int id=0;
			pid=1;
			while(pid>0 && id<next)
			{
				pid= waitpid(bgpid[id], &status, WNOHANG);
				if (pid!= -1 && pid!=0) 
				{
					//if the return status is 0 then only the process is exicted normally
					int pro=0;
					for(pro=0;pro<next;pro++)
					{
						if(bgpid[pro] == pid)
							break;
					} 
					// printf("%d %s\n",bgpid[0],bg_process[0]);
					// if(pid>0)
					if(status==0)
						printf("Process %s with pid = %d has exited normally\n",bg_process[pro],pid);
				}
				id++;
				pid=1;
				status=0;
			}
			k++;
		}
	}
	return 0;
}