# SHELL (Interactive user defined shell)

### Instructions for running the shell program:
---------------------------------------------------------------------------------------------------

> -  make
> - ./a.out

---------------------------------------------------------------------------------------------------
## Commands support by the shell:

1) cd 
> - cd (path) {changes the current directory to the directory whise path has been provided .Path can be either relative or absolute.}
> - cd ~ {changes to the default directory(where the file is run)}
> - cd ..  {changes to parent directory}

2) pwd
> - the current working directory

3) echo "text to be printed"
> - prints the text as it is as provided

4) ls 
> - gives information about the current directory
> - flags implemented :: -l, -a

5) pinfo pid 
> - pid
> - process status
> - virtual memory
> - executable path 

6) background(ends with '&') and foreground processes

7) clock -t <interval> -n <duration>
> - displays the time and date dynamically after every interval uptil duration that is provided

8) remindme <time interval> "<label>"
> - sets a reminder and after the provided time interval the label is displayed.

## FILES INCLUDED::
1 main.c
> - The main process which calls all the other program to execute the respective programs. 
> - Takes the input. 
> - Checks all the child process if they have exited or not

2 builtin_command.c
> - Contains the function for the builtin command implemented.
> - builtin commands:: echo ,pwd ,cd

3 ls_command.c
> - Contains the function for executing ls commands and its flags.

4 bg.c
> - contains the function for executing a different program using execvp.

5 pinfo.c
> - contains function for display the process id,status,virtual memory and executable path.

6 reminder.c
> - contains function for executing the reminder command.

7 clock.c
> - contains function for executing the clock command.

8 <filename.h>
> - Header files of the respective file name.
