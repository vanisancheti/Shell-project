# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <string.h>
# define _GNU_SOURCE
void pinfo(char* pid)
{
    FILE *stream;
    char *line=NULL;
    size_t len=0;
    ssize_t read;
    // char* pid;
    //writing the pid into a string 
    // printf("%s\n",pid);
    char Pro_status;
    char *vmem;
    char *exe_path;
    char file_name[1024];

    //For the virtual memory size /proc/[pid]/status file is used
    strcpy(file_name,"/proc/");
    strcat(file_name,pid);
    strcat(file_name,"/status");

    stream = fopen(file_name, "r");

    if(stream == NULL)
    {
      printf("stream is NULL\n");
      return ;
    }

    while ((read = getline(&line, &len, stream))!= -1) 
    {
        char* separate;
        separate = strtok(line,"\n:\t ");
        
        while(separate!=NULL && strcmp(separate,"VmSize")!=0 )
          separate=strtok(NULL,"\n:\t ");
        
        if(separate!=NULL && strcmp(separate,"VmSize")==0)
        { 
          separate = strtok(NULL,"\n:\t ");
          if(separate!=NULL)
            vmem = strdup(separate);//it allocates memory and the copies the string
            break; 
        }
    }
    fclose(stream);
    //For the process status ,executable path ,pid /proc/[pid]/stat file is used
    strcpy(file_name,"/proc/");
    strcat(file_name,pid);
    strcat(file_name,"/stat");
    // printf("%s\n",file_name);
    stream = fopen(file_name, "r");
    if (stream == NULL)
    {
      printf("stream is NULL\n");
      return ;
    }
    while ((read = getline(&line, &len, stream)) != -1) 
    {
        char* separate;        
        separate = strtok(line," \t");
        separate = strtok(NULL," \t");
        char path[1024];
        path[0]='\0';
        if(separate!=NULL && separate[0]=='(')
        {
            // printf("%s\n",separate);
          while(separate!=NULL)
          {
            strcat(path,separate);
            strcat(path," ");

            if(separate[strlen(separate)-1]==')')
              break;
            separate =strtok(NULL," \t");
          }
        }
        //removing the '(' and ')'
        // printf("%s\n",path);
        strncpy(path,path+1,strlen(path)-2);
        path[strlen(path)-3]='\0';
        exe_path = strdup(path);
        // printf("%s\n",exe_path);
        separate = strtok(NULL," \t");
        if(separate!=NULL)
          Pro_status = separate[0];
    }
    free(line);
    fclose(stream);
    printf("pid--%s\n",pid);
    printf("Process Status -- %c\n",Pro_status);
    printf("-%s{Virtual memory}\n",vmem);
    printf("-Executable Path - %s\n",exe_path);
    return ;       
}
