#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <time.h>
#include "globalvar.h"
void ls()
{
	for(int j=1;j<ind;j++)
	{
		if(command[j][0]=='-')
		{
			// storing all the flags in the flag array
			int len = strlen(command[j]);
			int prev_len= strlen(flag);
			for(int i=1;i<len;i++)
			{
				flag[prev_len+i-1]=command[j][i];
			}
			flag[prev_len+len-1]='\0';
		}
	}

	dirstream = opendir(currdir_abs);
	direc = readdir(dirstream);
	if(strlen(flag)==0)
	{
		while(direc!=NULL)
		{
			if(strcmp(direc->d_name,"..")!=0 && strcmp(direc->d_name,".")!=0)
				printf("%s\n",direc->d_name);
			direc = readdir(dirstream);
		}
	}
	else
	{
		struct stat sb;
		struct passwd* user;
		struct passwd* group;
		if(strcmp(flag,"l")==0 || strcmp(flag,"al")==0 || strcmp(flag,"la")==0)
		{

			while(direc!=NULL)
			{	
				if(strcmp(flag,"l")==0)
				{
					if(strcmp(direc->d_name,"..")==0 || strcmp(direc->d_name,".")==0)
					{
						direc=readdir(dirstream);
						continue;
					}
				}
				char name[1024];
				strcpy(name,currdir_abs);
				strcat(name,"/");
				strcat(name,direc->d_name);

				stat(name,&sb);
				char permission[1000];
				int l=0; 
				
				if(S_ISDIR(sb.st_mode))
					permission[l]='d';
				else
					permission[l]='-';
				
				l++;

				if(sb.st_mode & S_IRUSR)
					permission[l]='r';
				else
					permission[l]='-';
				l++;
				
				if(sb.st_mode & S_IWUSR)
					permission[l]='w';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IXUSR)
					permission[l]='x';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IRGRP)
					permission[l]='r';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IWGRP)
					permission[l]='w';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IXGRP)
					permission[l]='x';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IROTH)
					permission[l]='r';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IWOTH)
					permission[l]='w';
				else
					permission[l]='-';
				l++;

				if(sb.st_mode & S_IXOTH)
					permission[l]='x';
				else
					permission[l]='-';
				l++;

				permission[l]='\0';
				//getting the name of the owner and the group through uid and gid
				user = getpwuid(sb.st_uid);
				group = getpwuid(sb.st_gid);
				// getting modified time of the file/directory 
				char time[50];
				strftime(time, 50, "%Y-%m-%d %H:%M", localtime(&sb.st_mtime));
				printf("%s %ld %s %s %ld %s %s\n",permission,(long)sb.st_nlink,user->pw_name,group->pw_name,(long)sb.st_size,time,direc->d_name);
				permission[0]='\0';
				direc = readdir(dirstream);
			}
		}
		if(strcmp(flag,"a")==0)
		{
			// direc=readdir(dirstream);
			while(direc!=NULL)
			{	
				printf("%s\n",direc->d_name);
				direc = readdir(dirstream);
			}
		}


	} 
}