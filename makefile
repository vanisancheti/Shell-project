program: main.o builtin_command.o ls_command.o pinfo.o clock.o bg.o	reminder.o
	gcc main.o builtin_command.o ls_command.o pinfo.o clock.o bg.o reminder.o

main.o: main.c globalvar.h builtin_command.h ls_command.h bg.h
	gcc -c main.c

builtin_command.o: builtin_command.c globalvar.h
	gcc -c builtin_command.c

ls_command.o: ls_command.c globalvar.h
	gcc -c ls_command.c 

bg.o: bg.c
	gcc -c bg.c

pinfo.o: pinfo.c
	gcc -c pinfo.c

reminder.o: reminder.c
	gcc -c reminder.c

clock.o: clock.c
	gcc -c clock.c